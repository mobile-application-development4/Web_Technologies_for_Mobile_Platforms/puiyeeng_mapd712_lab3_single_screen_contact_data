import React, { useState } from "react";
import { StyleSheet, Text, View ,TextInput,TouchableOpacity,Alert} from 'react-native';

export default function App() {
  const [firstName, setFirstName] = 	React.useState('');  
  const [lastName, setLastName] = 	React.useState(''); 
  const [email, setEmail] = React.useState(''); 

  const handleSend = () => {
    Alert.alert('Contact Sent', 'Hi '+ firstName + ' ' + lastName +', Your contact is successfully saved!');;


  }

  const handleClear = () => {
      setFirstName('');
      setLastName('');
      setEmail('');
      Alert.alert('Input Cleared');;

  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Contact Form</Text>
      
      <Text style={styles.label}>First Name:</Text>

      <TextInput style={styles.textInput} 
      placeholder="Your First Name"
      onChangeText={text => setFirstName(text)}
      value={firstName} 
      />

      <Text style={styles.label}>Last Name:</Text>
      <TextInput style={styles.textInput} 
      placeholder="Your Last Name" 
      onChangeText={text => setLastName(text)}
      value={lastName}
      />

      <Text style={styles.label}>Email:</Text>
      <TextInput style={styles.textInput} 
      placeholder="Your Email" 
      onChangeText={text => setEmail(text)}
      value={email}
      />

      <View style={styles.buttonContainer}>

      <TouchableOpacity style={styles.button}
      onPress={() => handleSend()}>
      <Text style={styles.buttonSendText}>Send</Text>
      
      </TouchableOpacity>

      <TouchableOpacity style={styles.button}
      onPress={() => handleClear()}
      >
      
      <Text style={styles.buttonClearText}>Clear</Text>
      </TouchableOpacity>
    </View>
    
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    fontSize: 30,
    color: 'black',
    borderColor:"black",
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: '#199187',
    borderBottomWidth: 1
    
  },
  label:{
    alignSelf: 'center',
    fontSize: 20,
    width: 300,
    height: 40,
  },
  textInput:{
    alignSelf: 'center',
    fontSize: 20,
    borderColor:"black",
    marginBottom: 30,
    color: 'black',
    width: 300,
    height: 40,
    borderWidth: 2,
    borderRadius: 100,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFFFE0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  button: {
    width: 120,
    height: 50,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  buttonSendText: {
    fontSize: 30,
    color: 'green',
  },
  buttonClearText: {
    fontSize: 30,
    color: 'red',
  },
});
